package FtpCommands;

import java.nio.file.Paths;

import FtpDataChannels.AbstractDataChannel;
import FtpDataChannels.IDataChannelMonitorListener;
import FtpDataChannels.UploadDataChannel;

public class CommandFactory {

	public static Command CreateEmptyCommand() {
		return new Command(FtpCommand.EMPTY, ResponseCode.SERVICE_READY_FOR_NEW_USER);
	}
	
	public static Command CreateUserCommand(String argument) {
		return new Command(FtpCommand.USER, argument, ResponseCode.USER_NAME_OKAY_NEED_PASSWORD);
	}

	public static Command CreatePassCommand(String argument) {
		return new Command(FtpCommand.PASS, argument, ResponseCode.USER_LOGGED_IN_PROCEED);
	}

	public static Command CreateTypeCommand(String argument) {
		return new Command(FtpCommand.TYPE, argument, ResponseCode.ACTION_COMPLETED_SUCCESSFULLY);
	}
	
	public static CommandPasv CreatePasvCommand() {
		return new CommandPasv(FtpCommand.PASV, ResponseCode.ENTERING_PASSIVE_MODE);
	}
	
	public static CommandWithDataChannel CreateStorCommand(String argument, CommandPasv commandPasv, IDataChannelMonitorListener listener) {
		CommandWithDataChannel cmd = new CommandWithDataChannel(FtpCommand.STOR, argument, ResponseCode.OPENING_DATA_CHANNEL, ResponseCode.CLOSING_DATA_CONNECTION_SUCCESS_TRANSFER);
		cmd.setPasvCommand(commandPasv);
		AbstractDataChannel dataChannel = new UploadDataChannel(Paths.get(argument));
		if (listener != null) {
			dataChannel.getListeners().add(listener);
		}
		cmd.setDataChannel(dataChannel);
		return cmd;
	}
	
	public static Command CreateQuitCommand() {
		return new Command(FtpCommand.QUIT, ResponseCode.SERVICE_CLOSING_CONTROL_CONNECTION);
	}

	public static Command CreateCwdCommand(String argument) {
		return new Command(FtpCommand.CWD, argument, ResponseCode.REQUESTED_FILE_ACTION_OKAY);
	}

}
