package FtpCommands;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.Arrays;

public class CommandPasv extends Command{

	private int[] dataChannelArgs;

	CommandPasv(FtpCommand command, ResponseCode ...acceptedCode) {
		super(command, acceptedCode);
	}
	
	public String GetAddress() {
		return dataChannelArgs[0] + "." + dataChannelArgs[1] + "." + dataChannelArgs[2] + "." + dataChannelArgs[3];
	}
	public int GetPort() {
		return dataChannelArgs[4] * 256 + dataChannelArgs[5];
	}
	
	@Override
	public void execute(SocketChannel socket) throws IOException, CommandFailedException {
		super.execute(socket);
		
		String data = commandResponse.substring(commandResponse.indexOf('(') + 1, commandResponse.indexOf(')'));	
		dataChannelArgs = Arrays.stream(data.split(",")).mapToInt(Integer::parseInt).toArray();
	}

}
