package FtpCommands;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import FtpDataChannels.AbstractDataChannel;
import FtpDataChannels.DataChannelResult;

public class CommandWithDataChannel extends Command{
	private AbstractDataChannel dataChannel;
	private CommandPasv pasvCommand;
	
	CommandWithDataChannel(FtpCommand command, String argument, ResponseCode ...acceptedCode) {
		super(command, Paths.get(argument).getFileName().toString(), acceptedCode);		
	}
		
	
	public void setPasvCommand(CommandPasv pasvCommand) {
		this.pasvCommand = pasvCommand;
	}


	public void setDataChannel(AbstractDataChannel dataChannel) {
		this.dataChannel = dataChannel;
	}


	public AbstractDataChannel getDataChannel() {
		return dataChannel;
	}


	@Override
	public void execute(SocketChannel socket) throws IOException, CommandFailedException {
		
		dataChannel.openConnection(pasvCommand.GetAddress(), pasvCommand.GetPort());
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		try {
			send(socket);
			
			receive(socket);
			
			Future<DataChannelResult> result = executorService.submit(dataChannel);		
			
			if (result.get() == DataChannelResult.SUCCESS) {
				receive(socket);
				dataChannel.CreateSuccessEvent();
			} else {
				dataChannel.CreateFailedEvent();;
			}
		} catch (InterruptedException | ExecutionException e) {
			dataChannel.CreateFailedEvent();
			throw new CommandFailedException(e.getMessage());
		} finally {
			dataChannel.closeConnection();
			executorService.shutdown();
		}
	}
}
