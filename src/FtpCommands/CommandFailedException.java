package FtpCommands;

public class CommandFailedException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public CommandFailedException(String errorMessage) {
        super(errorMessage);
    }
}
