package FtpCommands;

public enum FtpCommand {
	EMPTY(""),
	USER("USER"),
	PASS("PASS"),
	SYST("SYST"),
	PASV("PASV"),
	TYPE("TYPE"),
	STOR("STOR"),
	MDTM("MDTM"),
	CWD("CWD"),
	QUIT("QUIT");
	
	public String value;
	private FtpCommand(String value) {
		this.value = value;
	}
}
