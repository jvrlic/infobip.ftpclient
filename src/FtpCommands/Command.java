package FtpCommands;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Arrays;

public class Command {
	private FtpCommand command;
	private String argument;
	protected ResponseCode[] acceptedCodes;
	protected String commandResponse;
	protected ByteBuffer bufferWrite;
	protected ByteBuffer bufferRead;
	
	private final int READ_BUFFER_SIZE = 256;

	public Command(FtpCommand command, String argument, ResponseCode ...acceptedCode) {
		this.command = command;
		this.argument = argument;
		this.commandResponse = null;
		this.acceptedCodes = acceptedCode;
		
		bufferWrite = ByteBuffer.allocate(this.toString().length());
		bufferWrite.clear();	
		bufferWrite.put(this.toString().getBytes());
		bufferWrite.flip();

		bufferRead = ByteBuffer.allocate(READ_BUFFER_SIZE);
	}

	
	public Command(FtpCommand command, ResponseCode ...acceptedCode) {
		this(command, null, acceptedCode);
	}
	
	protected void send(SocketChannel socket) throws IOException {
		while (bufferWrite.hasRemaining()) {
			socket.write(bufferWrite);
		}
	}
	protected void receive(SocketChannel socket) throws IOException, CommandFailedException {
		
		bufferRead.clear();
		int bytesRead = socket.read(bufferRead);
		bufferRead.flip();	
		//System.out.println(bytesRead + ": " + new String(bufferRead.array()));
		
		if (bytesRead > 0) {
			for (String r: new String(bufferRead.array()).split("\\r?\\n")) {
				if (SuccessCommandResponse(r)) {
					commandResponse = r;
					return;
				}
			}
		}
		throw new CommandFailedException("FTP command " + command + " failed. Response was: " + new String(bufferRead.array()));
	}
	
	public void execute(SocketChannel socket) throws IOException, CommandFailedException {		
		send(socket);
	
		receive(socket);		
	}
	
	private boolean SuccessCommandResponse(String response) {
		String [] strs = response.split("\\s+");
		if (strs.length != 0) {
			try {
				if (Arrays.stream(acceptedCodes).anyMatch(c -> c.value == Integer.parseInt(strs[0]))) {
					return true;
				}
			}
			catch (NumberFormatException nfe) {
				return false;
			}
		}
		return false;
	}

	
	@Override
	public String toString() {
		if (argument == null) {
			return command.value + System.lineSeparator();
		}
		
		return command.value + " " + argument + System.lineSeparator();
	}
}
