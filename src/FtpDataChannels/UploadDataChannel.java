package FtpDataChannels;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.file.Files;
import java.nio.file.Path;

import FtpCommands.CommandFailedException;

public class UploadDataChannel extends AbstractDataChannel{	
	protected final int SEND_CHUNK_SIZE = 10240;

	public UploadDataChannel(Path filePath) {
		super(filePath);
	}

	@Override
	public DataChannelResult call() throws IOException, CommandFailedException{
		Selector selector = Selector.open();
			
		socket.configureBlocking(false);
			  
		byte[] fileContent = Files.readAllBytes(filePath);
		fileTotalBytes = fileContent.length;
				
		ByteBuffer buf = ByteBuffer.allocate(SEND_CHUNK_SIZE);
		buf.clear();
			
		buf.put(fileContent, 0, SEND_CHUNK_SIZE);
		buf.flip();
			
		startTime = System.currentTimeMillis();
		while (buf.hasRemaining()) {
				
			int transferedBytes = socket.write(buf);
			fileTransferedBytes += transferedBytes;
				
			if (transferedBytes == 0) {
				CreateProgressEvent();
					
				socket.register(selector, SelectionKey.OP_WRITE);
				while (true) {
					int readyCount = selector.select();
	
					if (readyCount == 0) {    
						continue;
					}
						
					selector.selectedKeys().clear();
					break;
				}	
			} else {
				buf.flip();
				buf.put(fileContent, fileTransferedBytes, Math.min(buf.limit(), fileTotalBytes - fileTransferedBytes));
				buf.flip();			
			}
		}		
		CreateProgressEvent();
				
		closeConnection();
		return DataChannelResult.SUCCESS;
	}
}
