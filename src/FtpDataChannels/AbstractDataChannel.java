package FtpDataChannels;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


public abstract class AbstractDataChannel implements Callable<DataChannelResult>{
	protected Path filePath;	
	protected SocketChannel socket;
	
	protected long startTime;
	protected int fileTransferedBytes;
	protected int fileTotalBytes;
			
	protected List<IDataChannelMonitorListener> listeners;
		
	public AbstractDataChannel(Path filePath) {
		super();
		this.filePath = filePath;
		this.listeners = new ArrayList<IDataChannelMonitorListener>();
	}
	public void openConnection(String address, int port) throws IOException {
		socket = SocketChannel.open(new InetSocketAddress(address, port));
	}
	
	public void closeConnection() throws IOException {
		socket.close();
	}
	public void CreateSuccessEvent() {
		for(IDataChannelMonitorListener listener: listeners) {
			listener.onCompletion((new MonitorReport(
					filePath.getFileName().toString(),
					fileTransferedBytes,
					fileTotalBytes,
					startTime,
					System.currentTimeMillis(),
					DataChannelState.FINISHED_SUCCESS)));
		}
	}
	
	public void CreateFailedEvent() {
		for(IDataChannelMonitorListener listener: listeners) {
			listener.onCompletion((new MonitorReport(
					filePath.getFileName().toString(),
					fileTransferedBytes,
					fileTotalBytes,
					startTime == 0 ? System.currentTimeMillis() : startTime,
					System.currentTimeMillis(),
					DataChannelState.FINISHED_FAILED)));
		}
	}
	
	protected void CreateProgressEvent() {
		for(IDataChannelMonitorListener listener: listeners) {
			listener.onProgress((new MonitorReport(
					filePath.getFileName().toString(),
					fileTransferedBytes,
					fileTotalBytes,
					startTime,
					System.currentTimeMillis(),
					DataChannelState.IN_PROGRESS)));
		}
	}

	public List<IDataChannelMonitorListener> getListeners() {
		return listeners;
	}
}
