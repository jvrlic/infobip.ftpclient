package FtpDataChannels;

public class MonitorReport {
	private String filename;
	private long transferedFileSize; 
	private long totalFileSize;

	private long startTime;
	private long reportTime;

	private DataChannelState state;
	 
	public MonitorReport(String filename, long transferedFileSize, long totalFileSize, long startTime, long reportTime, DataChannelState state) {
		super();
		this.filename = filename;
		this.transferedFileSize = transferedFileSize;
		this.totalFileSize = totalFileSize;
		this.startTime = startTime;
		this.reportTime = reportTime;
		this.state = state;
	}

	public MonitorReport(String filename) {
		this(filename, 0, 0, 0, 0, DataChannelState.NOT_STARTED);
	}

	public long getTransferedFileSizeKB() {
		return transferedFileSize / 1024;
	}

	public long getTotalFileSizeKB() {
		return totalFileSize / 1024;
	}

	public String getFilename() {
		return filename;
	}

	public DataChannelState getState() {
		return state;
	}

	public void setReportTimeNow() {
		this.reportTime = System.currentTimeMillis();
	}

	public double getTransferDuration() {
		return reportTime != startTime ? (reportTime - startTime) / 1000. : 0;
	}
	
	public double getTransferSpeed() {
		return (transferedFileSize / 1024.) / getTransferDuration();
	}
	
	public int getTransferPercent() {
		return (int) (100 * transferedFileSize / totalFileSize);
	}
}
