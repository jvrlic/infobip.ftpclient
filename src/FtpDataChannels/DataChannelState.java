package FtpDataChannels;

public enum DataChannelState {
	NOT_STARTED,
	IN_PROGRESS,
	FINISHED_SUCCESS,
	FINISHED_FAILED
}
