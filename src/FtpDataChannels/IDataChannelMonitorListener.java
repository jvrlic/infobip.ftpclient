package FtpDataChannels;

public interface IDataChannelMonitorListener {
	void onCompletion(MonitorReport report);
	void onProgress(MonitorReport report);
}
