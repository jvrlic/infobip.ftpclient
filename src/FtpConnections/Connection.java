package FtpConnections;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import FtpCommands.*;
import FtpDataChannels.DataChannelState;
import FtpDataChannels.IDataChannelMonitorListener;
import FtpDataChannels.MonitorReport;

public class Connection implements Callable<Boolean>{
	private String address;
	private String filename;
	
	private ArrayList<Command> uploadCommandsControl;
	
	private MonitorReport dataChannelReport;
	
	private final int FTP_SERVER_PORT = 21;	
	
	
	public Connection(String address, String username, String password, String filename) {
		super();
		this.address = address;
		this.filename = filename;

		
		CommandPasv pasv = null;
		this.uploadCommandsControl = new ArrayList<Command>();
		this.uploadCommandsControl.add(CommandFactory.CreateEmptyCommand());
		this.uploadCommandsControl.add(CommandFactory.CreateUserCommand(username));
		this.uploadCommandsControl.add(CommandFactory.CreatePassCommand(password));
		//this.uploadCommandsControl.add(CommandFactory.CreateCwdCommand("upload"));		
		this.uploadCommandsControl.add(CommandFactory.CreateTypeCommand("I"));
		this.uploadCommandsControl.add(pasv = CommandFactory.CreatePasvCommand());
		this.uploadCommandsControl.add(CommandFactory.CreateStorCommand(filename,  pasv, new IDataChannelMonitorListener() {
			
			@Override
			public void onProgress(MonitorReport report) {
				dataChannelReport = report;
			}
			
			@Override
			public void onCompletion(MonitorReport report) {
				dataChannelReport = report;
			}
		}));
		this.uploadCommandsControl.add(CommandFactory.CreateQuitCommand());	
		
		this.dataChannelReport = new MonitorReport(Paths.get(filename).getFileName().toString());
	}
		
	
	public MonitorReport getDataChannelReport() {
		if (dataChannelReport.getState() == DataChannelState.IN_PROGRESS) {
			dataChannelReport.setReportTimeNow();
		}
		return dataChannelReport;
	}

	public String getFilename() {
		return Paths.get(filename).getFileName().toString();
	}

	@Override
	public Boolean call() {
		SocketChannel socket = null;
		try {
			socket = SocketChannel.open(new InetSocketAddress(address, FTP_SERVER_PORT));
			for(Command command: uploadCommandsControl) {
				command.execute(socket);
			}
			
		} catch (IOException e) {
			return false;
		} catch(CommandFailedException cfe) {
			return false;
		}
		finally {
			try {
				socket.close();
			} catch (IOException e) {
				return false;
			}
		}
		return true;
	}	
}
