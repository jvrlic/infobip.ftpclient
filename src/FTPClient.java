import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import FtpConnections.Connection;
import FtpDataChannels.DataChannelState;
import FtpDataChannels.MonitorReport;

public class FTPClient {

	final static int FILE_PRINT_LENGTH = 20;
	final static int ALL_TRANSFERS_TIMEOUT = 300;
	final static int REFRESH_RATE = 1;
	
	final static String DEFAULT_ADDRESS = "127.0.0.1";
	final static String DEFAULT_USERNAME = "user";
	final static String DEFAULT_PASSWORD = "pass";
	
	final static String[] ARG_PATTERNS = new String[] {
			"(-u\\s[a-zA-Z0-9._-]{3,}\\s)?",
			"(-p\\s[a-zA-Z0-9._-]{3,}\\s)?",
			"(-address\\s(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\s)?",
			"-files\\s(([a-zA-Z]:)?([\\\\/]?[a-z����A-Z���Ǝ0-9\\\\\\s_.-]+);?){1,5}"
	};

	
	private static String cutString(String str, int n) {
		if (str.length() > n) {
			return str.substring(0, n - 3) + "...";
		}
		return str;
	}
	
	private static String createFormattedDataString(List<Connection> connections, int length) {
		StringBuilder sb = new StringBuilder();
		Formatter fmt = new Formatter(sb);
		int speedLength = length - 12; // 10 = "KB/s" + " " + "s" + 5(duration) + 1(state) 
		
		for(Connection con: connections){
			MonitorReport dataChannelReport = con.getDataChannelReport();
			switch(dataChannelReport.getState()) {
			case IN_PROGRESS:
				fmt.format(">%" + speedLength + ".1fKB/s %5.0fs|", dataChannelReport.getTransferSpeed(), dataChannelReport.getTransferDuration());
				break;
			case NOT_STARTED:
				fmt.format("%" + length + "s|", "NOT STARTED");				
				break;
			case FINISHED_SUCCESS:
				fmt.format("+%" + speedLength + ".1fKB/s %5.0fs|", dataChannelReport.getTransferSpeed(), dataChannelReport.getTransferDuration());
				//fmt.format("%" + length + "s|", "SUCCESS");				
				break;
			case FINISHED_FAILED:
				fmt.format("-%" + speedLength + ".1fKB/s %5.0fs|", dataChannelReport.getTransferSpeed(), dataChannelReport.getTransferDuration());
				//fmt.format("%" + length + "s|", "FAILED");				
				break;
			}
		}
		fmt.close();
		return sb.toString();
	}

	private static String createFormattedHeaderString(List<Connection> connections, int length) {
		StringBuilder sb = new StringBuilder();
		Formatter fmt = new Formatter(sb);
		
		connections.forEach(c -> fmt.format("%-" + length + "s|", cutString(c.getFilename(), length)));
		
		fmt.close();
		return sb.toString();
	}
	
	private static String createFormattedTotalString(List<Connection> connections) {
		StringBuilder sb = new StringBuilder();
		Formatter fmt = new Formatter(sb);
		
		double maxDuration = connections
				.stream()
				.filter(c -> c.getDataChannelReport().getState() != DataChannelState.NOT_STARTED)
				.mapToDouble(c -> c.getDataChannelReport().getTransferDuration())
				.max()
				.orElse(0);
		
		double totalDuration = connections
				.stream()
				.filter(c -> c.getDataChannelReport().getState() != DataChannelState.NOT_STARTED)
				.mapToDouble(c -> c.getDataChannelReport().getTransferDuration())
				.sum();
		
		double averageSpeed = connections
				.stream()
				.filter(c -> c.getDataChannelReport().getState() != DataChannelState.NOT_STARTED)
				.mapToDouble(c -> c.getDataChannelReport().getTransferedFileSizeKB())
				.sum() / totalDuration;
		
		fmt.format("Files transfered in %.1f sec with average upload bandwidth %.2f KB/s", maxDuration, averageSpeed);
		
		fmt.close();
		return sb.toString();
	}

	private static void printUsage() {
		System.out.println();
		System.out.println("Usage: java FTPClient [-u username] [-p password] [-server address] -files nfiles");
		System.out.println();
		System.out.println("\tArgument:");
		System.out.println("\t\t-files\t\tList of maximum 5 files delimited with \";\"");
		System.out.println();
		System.out.println("\tOptions:");
		System.out.println("\t\t-u\t\tFTP account username. Default: \"user\"");
		System.out.println("\t\t-p\t\tFTP account password. Default: \"pass\"");
		System.out.println("\t\t-address\tFTP server address. Default: \"localhost\"");
	}
	
	public static void main(String[] args) {
		if (args.length < 2) {
			printUsage();
			System.exit(-1);
		}
		
		String arguments = String.join(" ", args);
		Pattern pattern = Pattern.compile("^" + String.join("", ARG_PATTERNS) + "$");

        Matcher matcher = pattern.matcher(arguments);
        if (!matcher.matches()) {
        	printUsage();
        	System.exit(-1);
        }
        
        HashMap<String, String> argMap = new HashMap<String, String>();
        argMap.put("u", DEFAULT_USERNAME);
        argMap.put("p", DEFAULT_PASSWORD);
        argMap.put("address", DEFAULT_ADDRESS);
    	Scanner scanner = new Scanner(arguments);
        for (String p: ARG_PATTERNS) {
    		String str = scanner.findInLine(p);
        	
    		if (!str.isEmpty()) {
    			String[] splitted = str.split(" ", 2);
    			argMap.put(splitted[0].substring(1), splitted[1].trim());
    		}
        }
        scanner.close();

        for (String file: argMap.get("files").split(";")) {
        	if (!Files.exists(Paths.get(file))) {
        		System.out.println("Some file(s) doesn't exist on filesystem.");
        		printUsage();
            	System.exit(-1);
        	}
        }
        		
		List<Connection> connections = new ArrayList<Connection>();		
		for (String file: argMap.get("files").split(";")) {
        	connections.add(new Connection(
        			argMap.get("address"),
        			argMap.get("u"),
        			argMap.get("p"),
        			file
        	));
        }
		
		
		System.out.println("\n" + createFormattedHeaderString(connections, FILE_PRINT_LENGTH));

		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				System.out.print(createFormattedDataString(connections, FILE_PRINT_LENGTH) + "\r");
			}}, 0, REFRESH_RATE, TimeUnit.SECONDS);
		
		
		ExecutorService executorService = Executors.newFixedThreadPool(connections.size());
		connections.forEach(c -> executorService.submit(c));
		
		executorService.shutdown();
	    try {
	        if (!executorService.awaitTermination(ALL_TRANSFERS_TIMEOUT, TimeUnit.SECONDS)) {
	        	executorService.shutdownNow();
	        }
	    } catch (InterruptedException ex) {
	    	executorService.shutdownNow();
	        Thread.currentThread().interrupt();
	    }
	    
		//scheduler.shutdown();
	    try {
	        if (!scheduler.awaitTermination(REFRESH_RATE, TimeUnit.SECONDS)) {
	        	scheduler.shutdownNow();
	        }
	    } catch (InterruptedException ex) {
	    	scheduler.shutdownNow();
	        Thread.currentThread().interrupt();
	    }
		
		System.out.println("\n");
		System.out.println(createFormattedTotalString(connections));
	}
}
