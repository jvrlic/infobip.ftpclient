Java application for parallel upload to FTP server maximum of 5 files.


java FTPClient [-u username] [-p password] [-server ip_address] -files file1.dat;file2.dat

    -u          FTP username (default: user)
    -p          FTP password (default: pass)
    -server     FTP server IP address (default: localhost)
    -files      Max 5 file paths for upload delimited with ;


